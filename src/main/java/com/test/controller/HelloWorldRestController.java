package com.test.controller;

import com.test.domain.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tom on 03/02/2017.
 */
@RestController
@RequestMapping("/hello")
public class HelloWorldRestController {

    @RequestMapping(value = "/uhirek", produces = "application/json", method = RequestMethod.POST)
    public String hello(@RequestBody User user) {
        return "hello world";
    }

}
